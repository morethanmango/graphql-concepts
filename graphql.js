const schemaDefinition = `
type Query {
  """Number of seconds elapsed since 01.01.1970"""
  unixTime: Int
}
`;

const resolvers = {
  Query: {
    unixTime: () => Math.floor(Date.now() / 1000),
  },
};

module.exports = { schemaDefinition, resolvers };
